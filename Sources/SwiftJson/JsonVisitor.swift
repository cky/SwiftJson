public protocol JsonVisitor {
    associatedtype Return
    func visit(null: Void) -> Return
    func visit(boolean: Bool) -> Return
    func visit(number: Float80) -> Return
    func visit(string: String) -> Return
    func visit(array: [JsonValue]) -> Return
    func visit(object: [String: JsonValue]) -> Return
}
