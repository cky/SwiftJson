public protocol JsonVisitable {
    func accept<Visitor: JsonVisitor>(_: Visitor) -> Visitor.Return
}
