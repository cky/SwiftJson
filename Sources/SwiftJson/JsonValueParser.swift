extension JsonValue: LosslessStringConvertible {
    public init?(_ description: String) {
        var start = description.startIndex
        guard let result = parse(description, pos: &start) else {
            return nil
        }
        self = result
    }
}

private func skipws(_ input: String, pos: inout String.Index) -> Bool {
    while pos != input.endIndex {
        switch input[pos] {
        case " ", "\t", "\n", "\r":
            input.formIndex(after: &pos)
        default:
            return true
        }
    }
    return false
}

private func scan(_ input: String, pos: inout String.Index, for token: String) -> Bool {
    guard skipws(input, pos: &pos) else {
        return false
    }
    var lpos = pos
    var rpos = token.startIndex
    while lpos != input.endIndex && rpos != token.endIndex {
        if input[lpos] != token[rpos] {
            return false
        }
        input.formIndex(after: &lpos)
        token.formIndex(after: &rpos)
    }
    guard rpos == token.endIndex else {
        return false
    }
    pos = lpos
    return true
}

private func scan(_ input: String, pos: inout String.Index, anyOf: String...) -> String? {
    guard skipws(input, pos: &pos) else {
        return nil
    }
    for token in anyOf {
        if scan(input, pos: &pos, for: token) {
            return token
        }
    }
    return nil
}

private let keywords: [String: JsonValue] = [
    "null": .null,
    "false": .boolean(false),
    "true": .boolean(true)
]

private func parse(_ input: String, pos: inout String.Index) -> JsonValue? {
    guard skipws(input, pos: &pos) else {
        return nil
    }

    if let keyword = scan(input, pos: &pos, anyOf: "null", "false", "true") {
        return keywords[keyword]
    }

    if scan(input, pos: &pos, for: "\"") {
        return parseString(input, pos: &pos).map(JsonValue.string)
    }

    if scan(input, pos: &pos, for: "[") {
        return parseArray(input, pos: &pos).map(JsonValue.array)
    }

    if scan(input, pos: &pos, for: "{") {
        return parseObject(input, pos: &pos).map(JsonValue.object)
    }

    return parseNumber(input, pos: &pos).map(JsonValue.number)
}

// This state machine allows the input to be validated against JSON's
// number format. We are not going to allow other kinds of numbers,
// even if supported by the system's number parser.
private enum ParseNumberState {
    case start, sign, zero, nonzero, dot, frac, e, expsign, exp, end

    func nextState(_ char: Character) -> ParseNumberState? {
        switch self {
        case .start:
            switch char {
            case "-": return .sign
            default: return ParseNumberState.sign.nextState(char)
            }

        case .sign:
            switch char {
            case "0": return .zero
            case "1"..."9": return .nonzero
            default: return nil
            }

        case .zero:
            switch char {
            case ".": return .dot
            case "e", "E": return .e
            default: return .end
            }

        case .nonzero:
            switch char {
            case "0"..."9": return self
            default: return ParseNumberState.zero.nextState(char)
            }

        case .dot:
            switch char {
            case "0"..."9": return .frac
            default: return nil
            }

        case .frac:
            switch char {
            case "0"..."9": return self
            case "e", "E": return .e
            default: return .end
            }

        case .e:
            switch char {
            case "-", "+": return .expsign
            default: return ParseNumberState.expsign.nextState(char)
            }

        case .expsign:
            switch char {
            case "0"..."9": return .exp
            default: return nil
            }

        case .exp:
            switch char {
            case "0"..."9": return self
            default: return .end
            }

        case .end:
            fatalError("Don't call nextState() on \(String(reflecting: self))")
        }
    }

    func isFinal() -> Bool {
        return self == .end
    }
}

private func parseNumber(_ input: String, pos: inout String.Index) -> Float80? {
    // Use a simple state machine to ensure number fits JSON format,
    // then delegate to system parser.
    var cur = pos
    var state = ParseNumberState.start
    while true {
        guard cur != input.endIndex else {
            // If we reach end of input, we'll pretend the string is
            // null-terminated and go from there. The next state must
            // be .end if the input was valid, or nil otherwise.
            guard let next = state.nextState("\0"),
                  next == .end else {
                return nil
            }
            break
        }

        guard let next = state.nextState(input[cur]) else {
            return nil
        }
        if next.isFinal() {
            break
        }
        state = next
        input.formIndex(after: &cur)
    }

    guard let number = Float80(input[pos..<cur]) else {
        return nil
    }
    pos = cur
    return number
}

private enum ParseStringState {
    case start, backslash, unicode(Int, UInt16),
         low_surrogate_backslash(UInt16), low_surrogate(Int, UInt16, UInt16),
         end

    func nextState(_ char: Character, result: inout String) -> ParseStringState? {
        switch self {
        case .start:
            switch char {
            case "\"": return .end
            case "\\": return .backslash
            default:
                result.append(char)
                return self
            }

        case .backslash:
            switch char {
            case "\"", "\\", "/": result.append(char)
            case "b": result.append("\u{8}")
            case "f": result.append("\u{c}")
            case "n": result.append("\n")
            case "r": result.append("\r")
            case "t": result.append("\t")
            case "u": return .unicode(4, 0)
            default: return nil
            }
            return .start

        case let .unicode(0, codePoint):
            guard let scalar = Unicode.Scalar(codePoint) else {
                return char == "\\" && UTF16.isLeadSurrogate(codePoint)
                        ? .low_surrogate_backslash(codePoint) : nil
            }
            result.append(Character(scalar))
            return ParseStringState.start.nextState(char, result: &result)

        case let .unicode(count, partial):
            guard let digit = UInt16(String(char), radix: 16) else {
                return nil
            }
            return .unicode(count - 1, (partial << 4) + digit)

        case let .low_surrogate_backslash(high):
            switch char {
            case "u": return .low_surrogate(4, high, 0)
            default: return nil
            }

        case let .low_surrogate(0, high, low):
            guard UTF16.isTrailSurrogate(low),
                  let scalar = Unicode.Scalar((UInt32(high & 0x3ff) << 10)
                            + UInt32(low & 0x3ff) + 0x10000) else {
                return nil
            }
            result.append(Character(scalar))
            return ParseStringState.start.nextState(char, result: &result)

        case let .low_surrogate(count, high, partial):
            guard let digit = UInt16(String(char), radix: 16) else {
                return nil
            }
            return .low_surrogate(count - 1, high, (partial << 4) + digit)

        case .end:
            fatalError("Don't call nextState() on \(String(reflecting: self))")
        }
    }

    func isFinal() -> Bool {
        switch self {
        case .end: return true
        default: return false
        }
    }
}

private func parseString(_ input: String, pos: inout String.Index) -> String? {
    var cur = pos
    var state = ParseStringState.start
    var result = ""
    while !state.isFinal() {
        guard cur != input.endIndex,
              let next = state.nextState(input[cur], result: &result) else {
            return nil
        }
        state = next
        input.formIndex(after: &cur)
    }
    pos = cur
    return result
}

private func parseArray(_ input: String, pos: inout String.Index) -> [JsonValue]? {
    if scan(input, pos: &pos, for: "]") {
        return []
    }
    var array = [JsonValue]()
    while true {
        guard let value = parse(input, pos: &pos),
              let next = scan(input, pos: &pos, anyOf: ",", "]") else {
            return nil
        }
        array.append(value)
        if next == "]" {
            break
        }
    }
    return array
}

private func parseObject(_ input: String, pos: inout String.Index) -> [String: JsonValue]? {
    if scan(input, pos: &pos, for: "}") {
        return [:]
    }
    var object = [String: JsonValue]()
    while true {
        guard scan(input, pos: &pos, for: "\""),
              let key = parseString(input, pos: &pos),
              scan(input, pos: &pos, for: ":"),
              let value = parse(input, pos: &pos),
              let next = scan(input, pos: &pos, anyOf: ",", "}") else {
            return nil
        }
        // Swift's Dictionary type doesn't expose the insert() method,
        // which was what I really wanted. An alternative is to call
        // updateValue(), which will still insert if the key doesn't
        // exist, and we can compare the return value against nil.
        guard object.updateValue(value, forKey: key) == nil else {
            return nil
        }
        if next == "}" {
            break
        }
    }
    return object
}
