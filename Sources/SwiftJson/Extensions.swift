extension LosslessStringConvertible where Self: TextOutputStreamable {
    public var description: String {
        var result = ""
        write(to: &result)
        return result
    }
}
