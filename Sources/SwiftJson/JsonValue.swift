public enum JsonValue {
    case null
    case boolean(Bool)
    case number(Float80)
    case string(String)
    indirect case array([JsonValue])
    indirect case object([String: JsonValue])
}

extension JsonValue: JsonVisitable {
    public func accept<Visitor: JsonVisitor>(_ visitor: Visitor) -> Visitor.Return {
        switch self {
        case .null: return visitor.visit(null: ())
        case let .boolean(value): return visitor.visit(boolean: value)
        case let .number(value): return visitor.visit(number: value)
        case let .string(value): return visitor.visit(string: value)
        case let .array(array): return visitor.visit(array: array)
        case let .object(object): return visitor.visit(object: object)
        }
    }
}

extension JsonValue: Equatable {
    public static func ==(lhs: JsonValue, rhs: JsonValue) -> Bool {
        switch (lhs, rhs) {
        case (.null, .null): return true
        case let (.boolean(value1), .boolean(value2)): return value1 == value2
        case let (.number(value1), .number(value2)): return value1 == value2
        case let (.string(value1), .string(value2)): return value1 == value2
        case let (.array(value1), .array(value2)): return value1 == value2
        case let (.object(value1), .object(value2)): return value1 == value2
        default: return false
        }
    }
}

extension JsonValue: TextOutputStreamable {
    public func write<Target: TextOutputStream>(to target: inout Target) {
        let visitor = JsonPrinterVisitor(target: target)
        accept(visitor)
        visitor.sync(target: &target)
    }
}

extension JsonValue: ExpressibleByNilLiteral {
    public init(nilLiteral: Void) {
        self = .null
    }
}

extension JsonValue: ExpressibleByBooleanLiteral {
    public init(booleanLiteral value: Bool) {
        self = .boolean(value)
    }
}

extension JsonValue: ExpressibleByFloatLiteral {
    public init(floatLiteral value: Float80) {
        self = .number(value)
    }
}

extension JsonValue: ExpressibleByIntegerLiteral {
    public init(integerLiteral value: Float80) {
        self = .number(value)
    }
}

extension JsonValue: ExpressibleByStringLiteral {
    public init(stringLiteral value: String) {
        self = .string(value)
    }
}

extension JsonValue: ExpressibleByArrayLiteral {
    public init(arrayLiteral elements: JsonValue...) {
        self = .array(elements)
    }
}

extension JsonValue: ExpressibleByDictionaryLiteral {
    public init(dictionaryLiteral elements: (String, JsonValue)...) {
        self = .object(Dictionary(uniqueKeysWithValues: elements))
    }
}
