open class JsonPrinterVisitor<Target: TextOutputStream>: JsonVisitor {
    private var target: Target

    public init(target: Target) {
        self.target = target
    }

    public func sync(target: inout Target) {
        target = self.target
    }

    public func write(_ value: String) {
        target.write(value)
    }

    public func hex(_ value: UInt16) -> String {
        let hexValue = String(value, radix: 16)
        let padding = String(repeating: "0", count: 4 - hexValue.count)
        return "\\u\(padding)\(hexValue)"
    }

    open func visit(null: Void) {
        write("null")
    }

    open func visit(boolean value: Bool) {
        write(value ? "true" : "false")
    }

    open func visit(number value: Float80) {
        write(String(value))
    }

    open func visit(string value: String) {
        write("\"")
        for scalar in value.unicodeScalars {
            write(encode(scalar))
        }
        write("\"")
    }

    open func visit(array: [JsonValue]) {
        var first = true
        write("[")
        for value in array {
            if first {
                first = false
            } else {
                write(",")
            }
            value.accept(self)
        }
        write("]")
    }

    open func visit(object: [String: JsonValue]) {
        var first = true
        write("{")
        for (key, value) in object {
            if first {
                first = false
            } else {
                write(",")
            }
            visit(string: key)
            write(":")
            value.accept(self)
        }
        write("}")
    }

    open func encode(_ scalar: Unicode.Scalar) -> String {
        switch scalar.value {
        case 8: return "\\b"
        case 9: return "\\t"
        case 10: return "\\n"
        case 12: return "\\f"
        case 13: return "\\r"
        case 34: return "\\\""
        case 92: return "\\\\"
        case 0..<32: return hex(scalar.utf16.first!)
        default: return String(scalar)
        }
    }
}
