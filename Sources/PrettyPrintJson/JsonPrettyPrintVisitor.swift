import SwiftJson

class JsonPrettyPrintVisitor<Target: TextOutputStream>: JsonPrinterVisitor<Target> {
    private let params: JsonPrettyPrintParams
    private var prefix: String = "\n"

    init(params: JsonPrettyPrintParams, target: Target) {
        self.params = params
        super.init(target: target)
    }

    override func visit(number value: Float80) {
        guard let int = Int64(exactly: value) else {
            return super.visit(number: value)
        }
        write(String(int))
    }

    override func visit(array: [JsonValue]) {
        guard !array.isEmpty else {
            return super.visit(array: array)
        }
        var first = true
        let outerPrefix = prefix
        prefix += params.indentString
        write("[")
        for value in array {
            if first {
                first = false
            } else {
                write(",")
            }
            write(prefix)
            value.accept(self)
        }
        prefix = outerPrefix
        write(prefix)
        write("]")
    }

    override func visit(object: [String: JsonValue]) {
        guard !object.isEmpty else {
            return super.visit(object: object)
        }
        var first = true
        let outerPrefix = prefix
        prefix += params.indentString
        write("{")
        for (key, value) in object.sorted(by: { $0.key < $1.key }) {
            if first {
                first = false
            } else {
                write(",")
            }
            write(prefix)
            visit(string: key)
            write(": ")
            value.accept(self)
        }
        prefix = outerPrefix
        write(prefix)
        write("}")
    }

    override func encode(_ scalar: Unicode.Scalar) -> String {
        guard params.asciiOnly && scalar.value >= 128 else {
            return super.encode(scalar)
        }
        return scalar.utf16.map(hex).joined()
    }
}

