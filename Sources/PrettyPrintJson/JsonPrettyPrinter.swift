import SwiftJson

class JsonPrettyPrinter: TextOutputStreamable {
    private let value: JsonValue
    private let params: JsonPrettyPrintParams

    init(_ value: JsonValue, indent: Int, asciiOnly: Bool = false) {
        self.value = value
        self.params = JsonPrettyPrintParams(
                indentString: String(repeating: " ", count: indent),
                asciiOnly: asciiOnly)
    }

    func write<Target: TextOutputStream>(to target: inout Target) {
        let visitor = JsonPrettyPrintVisitor(params: params, target: target)
        value.accept(visitor)
        visitor.sync(target: &target)
    }
}
