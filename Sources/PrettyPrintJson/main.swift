import SwiftJson

var lines = [String]()
while let line = readLine(strippingNewline: false) {
    lines.append(line)
}
guard let parsed = JsonValue(lines.joined()) else {
    fatalError("Unable to parse as JSON")
}

print(JsonPrettyPrinter(parsed, indent: 2))
