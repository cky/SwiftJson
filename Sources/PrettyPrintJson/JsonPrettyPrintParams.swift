struct JsonPrettyPrintParams {
    let indentString: String
    let asciiOnly: Bool
}
