import XCTest
import SwiftJson

public class SwiftJsonTests: XCTestCase {
    private func assertSerialises(_ value: JsonValue, to expected: String) {
        var json = String(describing: value)
        // Special hack for the object serialisation test to sort all
        // the entries so the results are consistent with our expected
        // strings.
        if json.starts(with: "{") {
            let range = json.index(after: json.startIndex)..<json.index(before: json.endIndex)
            let sorted = json[range].split(separator: ",").sorted().joined(separator: ",")
            json = "{\(sorted)}"
        }
        XCTAssertEqual(json, expected)
    }

    private func assertCorrectRoundtrip(_ value: JsonValue) {
        let json = String(describing: value)
        if let actual = JsonValue(json) {
            XCTAssertEqual(actual, value)
        } else {
            XCTFail("Failed to parse \(json)")
        }
    }

    public func testSerialisation() {
        assertSerialises(nil, to: "null")
        assertSerialises(false, to: "false")
        assertSerialises(true, to: "true")
        assertSerialises(42, to: "42.0")
        assertSerialises("\0\u{8}\t\n\u{c}\r\u{1f}\u{20}\"\\",
                to: "\"\\u0000\\b\\t\\n\\f\\r\\u001f \\\"\\\\\"")
        assertSerialises(["array", "roundtrip", "test"],
                to: "[\"array\",\"roundtrip\",\"test\"]")
        assertSerialises(["object": [], "roundtrip": [:], "test": 3],
                to: "{\"object\":[],\"roundtrip\":{},\"test\":3.0}")
        assertSerialises([["nested": "object"]],
                to: "[{\"nested\":\"object\"}]")
        assertSerialises(["nested": ["array"]],
                to: "{\"nested\":[\"array\"]}")
    }

    public func testRoundtrip() {
        assertCorrectRoundtrip(nil)
        assertCorrectRoundtrip(false)
        assertCorrectRoundtrip(true)
        assertCorrectRoundtrip(42)
        assertCorrectRoundtrip("\0\u{8}\t\n\u{c}\r\u{1f}\u{20}\"\\")
        assertCorrectRoundtrip(["array", "roundtrip", "test"])
        assertCorrectRoundtrip(["object": [], "roundtrip": [:], "test": 3])
        assertCorrectRoundtrip([["nested": "object"]])
        assertCorrectRoundtrip(["nested": ["array"]])
    }

    public static var allTests = [
        ("testSerialisation", testSerialisation),
        ("testRoundtrip", testRoundtrip)
    ]
}
