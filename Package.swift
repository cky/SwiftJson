// swift-tools-version:4.0
import PackageDescription

let package = Package(
        name: "SwiftJson",
        products: [
            .library(name: "SwiftJson", targets: ["SwiftJson"]),
            .executable(name: "PrettyPrintJson", targets: ["PrettyPrintJson"])
        ],
        targets: [
            .target(name: "SwiftJson", dependencies: []),
            .target(name: "PrettyPrintJson", dependencies: ["SwiftJson"]),
            .testTarget(name: "SwiftJsonTests", dependencies: ["SwiftJson"])
        ])
